<?php
include('session.php'); 
include('function.php');
global $con;

if(!isset($_SESSION['user_id']))
{
  header('Location:index.php');
}
if(isset($_POST['btn_submit']))
{
   $_SESSION['exam']=$_POST;
   $str="";
   foreach($_POST['topics'] as $key=>$val){$str.="'".$val."',";}
   $frest= rtrim($str,',');
   $subject=$_POST['subject'];
   $selct="select count(*) as cnt from tbl_question_bank where topic IN (".$frest.") and subject_id='".$subject."'";
   $query_1=mysqli_query($con,$selct);
   $row=mysqli_fetch_assoc($query_1);
   $total_question=$row['cnt'];
   $total_percentage=0;
   $total_percentage=($total_question/100);
   $total_easy_question=0;
   $total_medium_question=0;
   $total_hard_question=0;
   $total=0;
   $easy_level=$_POST['easy_level'];
   $medium_level=$_POST['medium_level'];
   $hard_level=$_POST['hard_level'];
   $main_array=array();
   if($_POST['descriptive_easy_marks_1']!="")
   {
    $main_array[]=get_questions($frest,$subject,'Easy','1',$_POST['descriptive_easy_marks_1'],'Descriptive');
   }
    if($_POST['descriptive_easy_marks_2']!="")
   {
     $main_array[]=get_questions($frest,$subject,'Easy','2',$_POST['descriptive_easy_marks_2'],'Descriptive');
   }
    if($_POST['descriptive_easy_marks_3']!="")
   {
    $main_array[]=get_questions($frest,$subject,'Easy','3',$_POST['descriptive_easy_marks_3'],'Descriptive');
   }
   if($_POST['descriptive_medium_marks_3']!="")
   {
    $main_array[]=get_questions($frest,$subject,'Medium','3',$_POST['descriptive_medium_marks_3'],'Descriptive');
   }
    if($_POST['descriptive_medium_marks_4']!="")
   {
    $main_array[]=get_questions($frest,$subject,'Medium','4',$_POST['descriptive_medium_marks_4'],'Descriptive');
   }
  if($_POST['descriptive_hard_marks_3']!="")
   {
    $main_array[]=get_questions($frest,$subject,'Hard','3',$_POST['descriptive_hard_marks_3'],'Descriptive');
   }

  if($_POST['descriptive_hard_marks_4']!="")
   {
    $main_array[]=get_questions($frest,$subject,'Hard','4',$_POST['descriptive_hard_marks_4'],'Descriptive');
   }

  if($_POST['descriptive_hard_marks_5']!="")
   {
    $main_array[]=get_questions($frest,$subject,'Hard','5',$_POST['descriptive_hard_marks_5'],'Descriptive');
   }

   if($_POST['descriptive_hard_marks_6']!="")
   {
    $main_array[]=get_questions($frest,$subject,'Hard','6',$_POST['descriptive_hard_marks_6'],'Descriptive');
   }

   if($_POST['multiple_easy_marks_1']!="")
   {
    $main_array[]=get_questions($frest,$subject,'Easy','1',$_POST['multiple_easy_marks_1'],'Multiple_choice');
   }

  if($_POST['numeric_easy_marks_2']!="")
   {
    $main_array[]=get_questions($frest,$subject,'Easy','2',$_POST['numeric_easy_marks_2'],'Numeric');
   }

   if($_POST['numeric_medium_marks_3']!="")
   {
    $main_array[]=get_questions($frest,$subject,'Medium','3',$_POST['numeric_medium_marks_3'],'Numeric');
   }

   if($_POST['numeric_hard_marks_4']!="")
   {
    $main_array[]=get_questions($frest,$subject,'Hard','4',$_POST['numeric_hard_marks_4'],'Numeric');
   }
   $data=array();
   foreach ($main_array as $key => $value) {
    foreach ($value as $key => $val) {
        $data[]=$val;
    }
   }
//print_r($t);
//print_r(array_merge($d));
//exit;

}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Exam</title>
<link rel="stylesheet" type="text/css" href="css/style.css">
</head>

<body>
	<table  class="wraper" border="0">
		<?php include('menu2.php'); ?>
		<tr>
		
			<td colspan="2" align="" height="505" valign="top" style="
    padding-left: 187px;
    padding-right: 50px;
"> 
				<h2>Summary </h2>
				 
				
				<form action="question_paper.php" method="post">
          <input type="hidden" value="<?php echo $_POST['subject']; ?>" name="subject_id" id="subject_id" >
          <input type="hidden" value="<?php echo $_POST['year']; ?>" name="year" id="year" >
          <input type="hidden" value="<?php echo $_POST['exam_type']; ?>" name="exam_type" id="exam_type" >
          <input type="hidden" value="<?php echo $_POST['number_of_marks']; ?>" name="max_marks" id="max_marks" >
          <input type="hidden" value="<?php echo implode(',',$_POST['topics']); ?>" name="topic" id="topic" >
          <input type="hidden" value="<?php echo $_POST['easy_level']; ?>" name="difficulty_level_easy" id="difficulty_level_easy" >
          <input type="hidden" value="<?php echo $_POST['medium_level']; ?>" name="difficulty_level_medium" id="difficulty_level_medium" >
          <input type="hidden" value="<?php echo $_POST['hard_level']; ?>" name="difficulty_level_hard" id="difficulty_level_hard" >
				 <div align="Left">
				<table class="table_login" width="960" height="133" border="0">
                  <tr>
                    <td >Subject</td>
                    <td colspan="7" ><?php 
                    $selcts="select * from tbl_subject where id='".$subject."'";
                   $query_s=mysqli_query($con,$selcts);
                   $row=mysqli_fetch_assoc($query_s);
                    echo $row['subject_name']; ?></td>
                  </tr>
                  <tr>
                    <td >Select Year</td>
                    <td colspan="7" > <?php echo $_POST['year']; ?></td>
                  </tr>
                  <tr>
                    <td >Exam</td>
                    <td colspan="7" ><?php echo $_POST['exam_type']; ?></td>
                  </tr>
                  <tr>
                    <td >Max Marks</td>
                    <td colspan="7" ><?php echo $_POST['number_of_marks']; ?></td>
                  </tr>
                  <tr>
                    <td colspan="8"><table class="table_login" width="100%" border="0"  >
                        <tr>
                          <td colspan="3" text-align="right"><strong>Syllabus Section</strong></td>
                        </tr>
                        <tr>
                          <td text-align="right">Topic</td>
                        </tr>
                        <tr>
                          <td text-align="right">
                          	<?php foreach ($_POST['topics'] as $key => $value) {
                              echo "<label>
                              ".$value."</label><br>";
                            } ?>

                          </td>
                         
                        </tr>
                    </table></td>
                  </tr>
           
                                    <tr>
                    <td>&nbsp;</td>
                    <td colspan="13"><span style="text-align: center;">How Many Questions? </span></td>
                  </tr>
                  <tr>
                    <td>Select Questions</td>
                    <td colspan="3"><div align="center">Easy</div></td>
                    <td colspan="2"><div align="center">Medium</div></td>
                    <td colspan="4"><div align="center">Hard</div></td>
                    <td> Total </td>
                  </tr>
                  <tr>
                    <td>Question Type</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
               
                  </tr>
                  <tr>
                    <td width="250">Marks</td>
                    <td width="60">1 Marks </td>
                    <td width="60">2 Marks</td>
                    <td width="60">3 Marks</td>
                    <td width="60">3 Marks</td>
                    <td width="60">4 Marks</td>
                    <td width="60">3 Marks</td>
                    <td width="60">4 Marks</td>
                    <td width="60">5 Marks</td>
                    <td width="60">6 Marks</td>
                    <td>&nbsp;</td>
                  </tr>
                  
                  <tr>
                    <td>Descriptive<input type="hidden" id="total_sub" value="0" name=""></td>
                    <td><?php echo $_POST['descriptive_easy_marks_1']; ?></td>
                    <td><?php echo $_POST['descriptive_easy_marks_2']; ?></td>
                    <td><?php echo $_POST['descriptive_easy_marks_3']; ?></td>
                    <td><?php echo $_POST['descriptive_medium_marks_3']; ?></td>
                    <td><?php echo $_POST['descriptive_medium_marks_4']; ?></td>
                    <td><?php echo $_POST['descriptive_hard_marks_3']; ?></td>
                    <td><?php echo $_POST['descriptive_hard_marks_4']; ?></td>
                    <td><?php echo $_POST['descriptive_hard_marks_5']; ?></td>
                    <td><?php echo $_POST['descriptive_hard_marks_6']; ?></td>
                    <td id="numeric" ><?php echo $_POST['descriptive_ts']; ?></td>
                  </tr>
                  <tr>
                    <td>Multiple Choice</td>
                    <td><?php echo $_POST['multiple_easy_marks_1']; ?></td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td id="descriptive"><?php echo $_POST['multipl_ts']; ?></td>
                  </tr>
                  <tr>
                    <td>Numeric</td>
                   <td>&nbsp;</td>
                    <td><?php echo $_POST['numeric_easy_marks_2']; ?></td>
                    <td>&nbsp;</td>
                    <td><?php echo $_POST['numeric_medium_marks_3']; ?></td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td> <?php echo $_POST['numeric_hard_marks_4']; ?></td>
                    <td id="multiple"><?php echo $_POST['numeric_ts']; ?></td>
                  </tr>
                  <tr>
                    <td>Total</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td id="final_total"><?php echo $_POST['final_totals']; ?></td>
                  </tr>
       
                
                                    <tr>
                    <td colspan="4"><strong>Difficulty Level</strong></td>
                    <td colspan="3"><strong>Total</strong></td>
                  </tr>
                  <tr>
                    <td>Easy</td>
                    <td colspan="3"><?php echo $_POST['easy_level']; ?>
                      %</td>
                      <td colspan="3"><input type="" value="<?php echo $_POST['easy_total']; ?>" readonly="readonly" name="easy_total" id="easy_total" />
                      </td>
                  </tr>
                  <tr>
                    <td>Medium</td>
                    <td colspan="3"><?php echo $_POST['medium_level']; ?>
                      %</td>
                      <td colspan="3"><input type="" value="<?php echo $_POST['medium_total']; ?>" readonly="readonly" name="medium_total" id="medium_total" />
                      </td>
                  </tr>
                  <tr>
                    <td>Hard</td>
                    <td colspan="3"><?php echo $_POST['hard_level']; ?>
                    %</td>
                    <td colspan="3"><input type="" value="<?php echo $_POST['hard_total']; ?>"  readonly="readonly" name="hard_total" id="hard_total" />
                      </td>
                  </tr>
                  <tr>

                    <td colspan="12"><h3>Questions</h3></td>
                  </tr>
                   <tr>
                    <td colspan="12">
					<table class="table_manage" width="100%"  border="0">
					<tr>
						<th>&nbsp;</th>
						<th>Question</th>
						<th>Syllabus Topic</th>
					    <th>Difficulty Level</th>
              <th>Question Type</th>
					    <th>Marks</th>
					</tr>
					<?php 
            $sum=0;
            $i=1;
            foreach($data as $value){      
            $sum=$sum+$value['max_possible_marks'];
            ?>
					<tr>
						<td><?php echo $i; ?>
            <input type="hidden" name="questions_id[]" value="<?php echo $value['question_id']; ?>">

            </td>
						<td><?php echo nl2br($value['question']); ?></td>
						<td><?php echo $value['topic']; ?></td>
						<td><?php echo $value['difficulty_level']; ?></td>
            <td><?php echo $value['question_type']; ?></td>
						<td><?php echo $value['max_possible_marks']; ?></td>
					</tr>
				<?php $i++; } ?>
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td>Total</td>

          <td><?php echo $sum; ?></td>

        </tr>
				</table>
				</td>
                  </tr>
				  
				  
				  <tr>
                    <td colspan="8">
					<div style="text-align: center;">
				  
	    <button name="btn_submit" type="submit"  style="width: 25%;height: 31px;margin-bottom: 19px;  margin-right: 10px;margin-top: 10px;">Generate Question Paper</button></div>
					</td>
					</tr>
                </table>
				 
				</div>
				</form>
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				 
			</td>
		</tr>
		 
		<tr class="tr_row">
			<td height="20" colspan="2" bgcolor="#9F6479" align="center"><span class="style11">Copyright &copy; 2019 College of Engineering, Pune</span></td>
		</tr>
		
		 
	</table>
</body>
</html>
