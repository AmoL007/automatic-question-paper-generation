<?php
include('session.php'); 
include('db.php');
global $con;
 $select="select tbl_subject.subject_name,tbl_syllabus.* from  tbl_syllabus join tbl_subject on tbl_subject.id=tbl_syllabus.subject_id";
 $query=mysqli_query($con,$select);
   if(isset($_GET['id']) && $_GET['action']=="delete")
	 {
	 	$select="DELETE FROM  tbl_syllabus WHERE syllabus_id=".base64_decode($_GET['id']);
	 	$query=mysqli_query($con,$select) ;
	 	if($query){
	 	   header('Location:manage_syllabus.php?msg=success&action=delete');
	 	}else{
	 		header('Location:manage_syllabus.php?msg=error&action=delete');
	 	}
	 }
	 if(isset($_GET['id']) && $_GET['action']=="status")
	 {
	 	$select="update tbl_syllabus set syllabus_status='".$_GET['status']."' WHERE syllabus_id=".base64_decode($_GET['id']);
	 	$query=mysqli_query($con,$select) ;
	 	if($query){
	 	   header('Location:manage_syllabus.php?msg=success&action=status');
	 	}else{
	 		header('Location:manage_syllabus.php?msg=error&action=status');
	 	}
	 }
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Exam</title>
<link rel="stylesheet" type="text/css" href="css/style.css">
</head>

<body>
	<table  class="wraper" border="0">
		<?php include('menu2.php'); ?>
		<tr width="100%">
			<?php include('left_sidebar_managesubject.php'); ?>
			<td  width="80%" height="505" valign="top" class="td_m" > 
				<h3>Manage Syllabus</h3>
				<button onclick="window.location='add_syllabus.php'" style="float: right;margin-bottom: 17px;" >Add New Syllabus</button>
					<?php if(isset($_GET['msg'])){ 
						$msg="";
								if($_GET['msg']=="success"){
									$action=$_GET['action'];
									if($action=="update"){
										$msg="User updated successfully";
									}else if($action=="add"){
										$msg="User added successfully";

									}else if($action=="delete"){
										$msg="User deleted successfully";
									}else if($action=="status")
									{
										$msg="User status change successfully";
									}
								?>
									<div class="alert success clearfix">
									  <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
									 <?php echo $msg; ?>
									</div>
						    <?php } }?>
				<form action="" method="post">
						<table class="table_manage" width="100%" style="margin-left: 2px; margin-right:2px; margin-bottom: 20px" border="0">
					<tr>
						<th>Subject</th>
						<th>Unit</th>
						<th>Topic</th>
						<th>Sub topic</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
					<?php while($row=mysqli_fetch_assoc($query)){ ?>
					<tr>
						<td><?php echo $row['subject_name']; ?></td>
						<td><?php echo $row['unit']; ?></td>
						<td><?php echo $row['topic']; ?></td>
						<td><?php echo $row['sub_topic']; ?></td>
						<td>
							<?php if($row['syllabus_status']=='1'){?>
								<a style="color:black;" href="manage_syllabus.php?id=<?php echo base64_encode($row['syllabus_id']); ?>&action=status&status=0">Active</a>
							<?php }else{ ?>
								<a style="color:black;" href="manage_syllabus.php?id=<?php echo base64_encode($row['syllabus_id']); ?>&action=status&status=1">Inactive</a>
							<?php } ?>

						</td>
						<td><a href="edit_syllabus.php?id=<?php echo base64_encode($row['syllabus_id']); ?>" style="cursor:pointer" ><img width="20" src="img/edit.png" /></a>&nbsp;&nbsp;<a href="manage_syllabus.php?id=<?php echo base64_encode($row['syllabus_id']); ?>&action=delete"><img width="20" src="img/delete.png" /></a></td>
					</tr>
				<?php } ?>

				</table>
	
				</form>
			</td>
		</tr>
		<tr class="tr_row">
			<td height="20" colspan="2" bgcolor="#9F6479" align="center"><span class="style11">Copyright &copy; 2019 College of Engineering, Pune</span></td>
		</tr>
	</table>
</body>
</html>
