<?php 
include('session.php'); 
include('db.php');
 global $con;
function test_input($data) {
            $data = trim($data);
            $data = stripslashes($data);
            $data = htmlspecialchars($data);
            return $data;
         }

function email_exist($email){


 }  

function user_exist($username){


 }        
$nameErr = $emailErr = $phoneErr= $passwrodErr= $usernameErr= $name = $email = $phone = $username = $passwrod = "";
 if(isset($_POST['btn_submit']))
 {
	if (empty($_POST["subject_id"])) {
               $nameErr = "Subject Id is required";
            }else {
               $subject_id = test_input($_POST["subject_id"]);
            }
    if (empty($_POST["subject_name"])) {
               $emailErr = "Subject Name is required";
            }else {
               $subject_name = test_input($_POST["subject_name"]);
            }

    if (empty($_POST["department"])) {
               $phoneErr = "Department is required";
            }else {
               $department = test_input($_POST["department"]);
            }

    if (empty($_POST["no_of_credits"])) {
               $userErr = "No of Credits is required";
            }else {
               $no_of_credits = test_input($_POST["no_of_credits"]);
            }
 		$update_query="update tbl_subject set subject_id='".$subject_id."',subject_name='".$subject_name."',department='".$department."',no_of_credits='".$no_of_credits."' WHERE id=".base64_decode($_GET['id']);

        if(mysqli_query($con,$update_query)){

        	header('Location:manage_subject.php?msg=success&action=update');
        }else{

        	header('Location:manage_subject.php?msg=error&action=update');
        }
 } 

 if(isset($_GET['id']))
 {
 	$select="select * from  tbl_subject where id=".base64_decode($_GET['id']);
 	$query=mysqli_query($con,$select);
 	$row=mysqli_fetch_assoc($query);
 }
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Exam</title>

<link rel="stylesheet" type="text/css" href="css/style.css">

</head>

<body>
	<table  class="wraper" border="0">
<?php include('menu2.php'); ?>
		<tr>
						<?php include('left_sidebar_managesubject.php'); ?>

			<td  height="505"  valign="top" > 
				<h1>Edit Subject</h1>
				<form action="" method="post">
				<table class="table_login" width="70%" border="0">
		
  					<tr>
						<td text-align="right">Subject ID </td>
						<td><input type="text" required="required" size="30" value="<?php echo $row['subject_id']; ?>" name="subject_id" id="subject_id" /></td>
					</tr>

						<tr>
						  <td text-align="right">Subject Name </td>
						  <td><input type="text" required="required" size="30" value="<?php echo $row['subject_name']; ?>" name="subject_name" id="subject_name" /></td>
				  </tr>
						<tr>
  					  <td text-align="right">Department</td>
  					  <td><select name="department" id="department">
                        <option <?php if($row['department']=='Civil Engineering Department'){ echo "selected"; } ?> >Civil Engineering Department</option>
                        <option <?php if($row['department']=='Computer Engineering and IT Department'){ echo "selected"; } ?> >Computer Engineering and IT Department</option>
                        <option <?php if($row['department']=='Electrical Engineering Department'){ echo "selected"; } ?> >Electrical Engineering Department</option>
						<option <?php if($row['department']=='Electronics and Telecommunication Engineering Department'){ echo "selected"; } ?> >Electronics and Telecommunication Engineering Department</option>
						<option <?php if($row['department']=='Instrumentation and Control Engineering Department'){ echo "selected"; } ?> >Instrumentation and Control Engineering Department</option>
						<option <?php if($row['department']=='Mechanical Engineering Department'){ echo "selected"; } ?> >Mechanical Engineering Department</option>
						<option <?php if($row['department']=='Metallurgy and Materials Science Department'){ echo "selected"; } ?> >Metallurgy and Materials Science Department</option>
						<option <?php if($row['department']=='Production Engineering Department'){ echo "selected"; } ?> >Production Engineering Department</option>
						<option <?php if($row['department']=='Department of Mathematics'){ echo "selected"; } ?> >Department of Mathematics</option>
						<option <?php if($row['department']=='Department of Applied Science'){ echo "selected"; } ?> >Department of Applied Science</option>
						<option <?php if($row['department']=='Department of Physics'){ echo "selected"; } ?> >Department of Physics</option>
									      
                      </select></td>
				  </tr>
  						<tr>
  					  <td text-align="right">No of Credits </td>
  					  <td><input type="text" required="required" size="30" value="<?php echo $row['no_of_credits']; ?>" name="no_of_credits" id="no_of_credits" /></td>
				  </tr>
					<tr>
						<td colspan="2"><button class="btn_mouse" name="btn_cancel"   type="button" style="width: 25%;height: 31px;">Cancel</button>&nbsp;<button class="btn_mouse" name="btn_submit" type="submit" style="width: 25%;height: 31px;">Submit</button> </td>
					</tr>
				</table>
				</form>
			</td>
		</tr>
		<tr class="tr_row">
			<td height="20" colspan="2" bgcolor="#9F6479" align="center"><span class="style11">Copyright &copy; 2019 College of Engineering, Pune</span></td>
		</tr>
	</table>
</body>
</html>
