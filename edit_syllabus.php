<?php 
include('session.php'); 
include('db.php');
 global $con;
function test_input($data) {
            $data = trim($data);
            $data = stripslashes($data);
            $data = htmlspecialchars($data);
            return $data;
         }
$nameErr = $emailErr = $phoneErr= $passwrodErr= $usernameErr= $name = $email = $phone = $username = $passwrod = "";
 if(isset($_POST['btn_submit']))
 {
	if (empty($_POST["subject_id"])) {
               $nameErr = "Name is required";
            }else {
               $subject_id = test_input($_POST["subject_id"]);
            }

    if (empty($_POST["unit"])) {
               $emailErr = "Email is required";
            }else {
               $unit = test_input($_POST["unit"]);
            }

    if (empty($_POST["topic"])) {
               $phoneErr = "Phone is required";
            }else {
               $topic = test_input($_POST["topic"]);
            }

    if (empty($_POST["sub_topic"])) {
               $userErr = "Username is required";
            }else {
               $sub_topic = test_input($_POST["sub_topic"]);
            }
 		$insert_query="update tbl_syllabus set subject_id='".$subject_id."',unit='".$unit."',topic='".$topic."',sub_topic='".$sub_topic."' WHERE syllabus_id=".base64_decode($_GET['id']);
        if(mysqli_query($con,$insert_query)){
        	header('Location:manage_syllabus.php?msg=success&action=update');
        }else{
        	header('Location:manage_syllabus.php?msg=error&action=update');
        }
 }

 if(isset($_GET['id']))
 {
 	$select="select * from  tbl_syllabus where syllabus_id=".base64_decode($_GET['id']);
 	$query=mysqli_query($con,$select);
 	$rows=mysqli_fetch_assoc($query);
 }
  $selectSub="select * from tbl_subject";
 $querysub=mysqli_query($con,$selectSub);
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Exam</title>

<link rel="stylesheet" type="text/css" href="css/style.css">

</head>

<body>
	<table  class="wraper" border="0">
<?php include('menu2.php'); ?>
		<tr>
			<?php include('left_sidebar_managesubject.php'); ?>
			<td  height="505"  valign="top" > 
				<h1>Edit Syllabus</h1>
				<form action="" method="post">
				<table class="table_login" width="70%" border="0">
					<tr>
						<td text-align="right">Subject</td>
						<td><select name="subject_id" id="subject_id" >
							<?php while($row=mysqli_fetch_assoc($querysub)) { ?>
								<option <?php if($rows['subject_id']==$row['id']){ echo 'selected' ;} ?> value="<?php echo $row['id']; ?>"><?php echo $row['subject_name']; ?> 
								</option>
							<?php } ?>
							</select>						
						</td>
					</tr>
					<tr>
						<td text-align="right">Unit</td>
						<td><input type="text" required="required" size="30" name="unit" id="unit" value="<?php echo $rows['unit']; ?>" /></td>
					</tr>
					<tr>
						<td text-align="right">Topic</td>
						<td><input type="text" required="required" size="30" value="<?php echo $rows['topic']; ?>" name="topic" id="topic" /></td>
					</tr>
					<tr>
						<td text-align="right">Sub Topic</td>
						<td><input type="text"  size="30" value="<?php echo $rows['sub_topic']; ?>" name="sub_topic" id="sub_topic" /></td>
					</tr>
					<tr>
						<td colspan="2"><button class="btn_mouse" name="btn_submit"   type="submit" style="width: 25%;height: 31px;">Cancel</button> &nbsp;<button class="btn_mouse" name="btn_submit" type="submit" style="width: 25%;height: 31px;">Submit</button> </td>
					</tr>
				</table>
				</form>
			</td>
		</tr>
		<tr class="tr_row">
			<td height="20" colspan="2" bgcolor="#9F6479" align="center"><span class="style11">Copyright &copy; 2019 College of Engineering, Pune</span></td>
		</tr>
	</table>
</body>
</html>
