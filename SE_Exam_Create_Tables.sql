-- Software System to Automate Examination Question Paper Generation
-- SQL Script to Create Tables
--

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+05:50";

-- --------------------------------------------------------
--
-- Database: db_college
--
-- --------------------------------------------------------

-- --------------------------------------------------------
--
-- Table structure for table tbl_question_bank
--
-- --------------------------------------------------------
CREATE TABLE tbl_question_bank (
  question_id int(11) NOT NULL,
  subject_id int(11) NOT NULL,
  question text NOT NULL,
  max_possible_marks int(11) NOT NULL,
  topic varchar(100) NOT NULL,
  unit varchar(100) NOT NULL,
  chapter varchar(100) NOT NULL,
  sub_topic varchar(100) NOT NULL,
  question_type varchar(100) NOT NULL,
  difficulty_level varchar(100) NOT NULL,
  pervious_year1 varchar(255) NOT NULL,
  question_status tinyint(1) NOT NULL,
  created_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  updated_date datetime NOT NULL,
  is_deleted tinyint(1) NOT NULL,
  pervious_year2 int(11) NOT NULL,
  pervious_year3 int(11) NOT NULL,
  pervious_year4 int(11) NOT NULL,
  pervious_year5 int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
-- --------------------------------------------------------

-- --------------------------------------------------------
--
-- Table structure for table tbl_question_paper
--
-- --------------------------------------------------------
CREATE TABLE tbl_question_paper (
  question_paper_id int(11) NOT NULL,
  subject_id int(11) NOT NULL,
  years int(11) NOT NULL,
  exam_type varchar(100) NOT NULL,
  max_marks int(11) NOT NULL,
  topic text NOT NULL,
  sub_topic text NOT NULL,
  number_of_numeric_questions int(11) NOT NULL,
  numeric_marks int(11) NOT NULL,
  number_of_descriptive_questions int(11) NOT NULL,
  descriptive_marks int(11) NOT NULL,
  number_of_multiple_choice_questions int(11) NOT NULL,
  multiple_choice_marks int(11) NOT NULL,
  difficulty_level_easy int(11) NOT NULL,
  difficulty_level_medium int(11) NOT NULL,
  difficulty_level_hard int(11) NOT NULL,
  questions_id varchar(255) NOT NULL,
  Number_of_Previous_Years_to_be_Excluded varchar(255) NOT NULL,
  marks_1 double NOT NULL,
  marks_2 double NOT NULL,
  marks_3 double NOT NULL,
  marks_4 double NOT NULL,
  marks_5 double NOT NULL,
  marks_6 double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
-- --------------------------------------------------------

-- --------------------------------------------------------
--
-- Table structure for table tbl_subject
--
-- --------------------------------------------------------

CREATE TABLE tbl_subject (
  id int(11) NOT NULL,
  subject_id varchar(100) NOT NULL,
  subject_name varchar(250) NOT NULL,
  department varchar(200) NOT NULL,
  no_of_credits int(11) NOT NULL,
  subject_status tinyint(1) NOT NULL,
  created_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  updated_date datetime NOT NULL,
  is_deleted tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
-- --------------------------------------------------------

-- --------------------------------------------------------
--
-- Table structure for table tbl_syllabus
--
-- --------------------------------------------------------
CREATE TABLE tbl_syllabus (
  syllabus_id int(11) NOT NULL,
  subject_id int(11) NOT NULL,
  chapter int(5) NOT NULL,
  unit varchar(50) NOT NULL,
  topic text NOT NULL,
  sub_topic text NOT NULL,
  syllabus_status tinyint(1) NOT NULL,
  created_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  update_date datetime NOT NULL,
  is_deleted tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
-- --------------------------------------------------------

-- --------------------------------------------------------
--
-- Table structure for table tbl_user
--
-- --------------------------------------------------------
CREATE TABLE tbl_user (
  user_id int(20) NOT NULL,
  fullname varchar(30) NOT NULL,
  passwords varchar(60) NOT NULL,
  user_status tinyint(1) NOT NULL,
  created_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  updated_date datetime NOT NULL,
  is_deleted tinyint(1) NOT NULL,
  email varchar(60) NOT NULL,
  phone varchar(12) NOT NULL,
  username varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------
--
-- Indexes for tables
--
-- --------------------------------------------------------

--
-- Indexes for table tbl_question_bank
--
ALTER TABLE tbl_question_bank
  ADD PRIMARY KEY (question_id);

--
-- Indexes for table tbl_question_paper
--
ALTER TABLE tbl_question_paper
  ADD PRIMARY KEY (question_paper_id);

--
-- Indexes for table tbl_subject
--
ALTER TABLE tbl_subject
  ADD PRIMARY KEY (id);

--
-- Indexes for table tbl_syllabus
--
ALTER TABLE tbl_syllabus
  ADD PRIMARY KEY (syllabus_id);

--
-- Indexes for table tbl_user
--
ALTER TABLE tbl_user
  ADD PRIMARY KEY (user_id);

-- --------------------------------------------------------
--
-- AUTO_INCREMENT for tables
--
-- --------------------------------------------------------
--
-- AUTO_INCREMENT for table tbl_question_bank
--
ALTER TABLE tbl_question_bank
  MODIFY question_id int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table tbl_question_paper
--
ALTER TABLE tbl_question_paper
  MODIFY question_paper_id int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table tbl_subject
--
ALTER TABLE tbl_subject
  MODIFY id int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table tbl_syllabus
--
ALTER TABLE tbl_syllabus
  MODIFY syllabus_id int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table tbl_user
--
ALTER TABLE tbl_user
  MODIFY user_id int(20) NOT NULL AUTO_INCREMENT;

