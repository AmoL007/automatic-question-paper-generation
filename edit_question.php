<?php
include('session.php');  
include('db.php');
 global $con;
function test_input($data) {
            $data = trim($data);
            $data = stripslashes($data);
            $data = htmlspecialchars($data);
            return $data;
         }
$nameErr = $emailErr = $phoneErr= $passwrodErr= $usernameErr= $name = $email = $phone = $username = $passwrod = "";
 if(isset($_POST['btn_submit']))
 {

			if (empty($_POST["subject_id"])) {
               $nameErr = "Name is required";
            }else {
               $subject_id = test_input($_POST["subject_id"]);
            }
    		if (empty($_POST["topic"])) {
               $emailErr = "Email is required";
            }else {
               $topic = implode(',',$_POST["topic"]);
            }
    		if (empty($_POST["sub_topic"])) {
               $phoneErr = "Phone is required";
            }else {
               $sub_topic = implode(',',$_POST["sub_topic"]);
            }
   			if (empty($_POST["question"])) {
               $userErr = "Username is required";
            }else {
               $question = test_input($_POST["question"]);
            }
           if (empty($_POST["max_possible_marks"])) {
               $userErr = "Username is required";
            }else {
               $max_possible_marks = test_input($_POST["max_possible_marks"]);
            }
            if (empty($_POST["difficulty_level"])) {
               $userErr = "Username is required";
            }else {
               $difficulty_level = test_input($_POST["difficulty_level"]);
            }  
            if (empty($_POST["question_type"])) {
               $userErr = "Username is required";
            }else {
               $question_type = test_input($_POST["question_type"]);
            }  
            $pervious_year1 = test_input($_POST["pervious_year1"]);
            $pervious_year2 = test_input($_POST["pervious_year2"]);
            $pervious_year3 = test_input($_POST["pervious_year3"]);
            $pervious_year4 = test_input($_POST["pervious_year4"]);
            $pervious_year5 = test_input($_POST["pervious_year5"]);
	 		$insert_query="update tbl_question_bank set subject_id='".$subject_id."',
	 		topic='".$topic."',
	 		sub_topic='".$sub_topic."',
	 		question='".$question."',
	 		max_possible_marks='".$max_possible_marks."',
	 		question_type='".$question_type."',
	 		difficulty_level='".$difficulty_level."',
	 		pervious_year1='".$pervious_year1."',
	 		pervious_year2='".$pervious_year2."',
	 		pervious_year3='".$pervious_year3."',
	 		pervious_year4='".$pervious_year4."',
	 		pervious_year5='".$pervious_year5."' where question_id=".base64_decode($_GET['id']);
	 		$query=mysqli_query($con,$insert_query) or die(mysqli_error($con));
	        if($query){

	        	header('Location:manage_questions.php?msg=success&action=update');
	        }else{

	        	header('Location:manage_questions.php?msg=error&action=update');
	        }
 }
 $select="select * from tbl_subject";
 $query=mysqli_query($con,$select);
 if(isset($_GET['id']))
 {
 	$id=base64_decode($_GET['id']);	
 	$select1="select * from tbl_question_bank where  question_id=".$id;
    $query1=mysqli_query($con,$select1);
    $rows=mysqli_fetch_assoc($query1);

    $topic_select="select * from tbl_syllabus where subject_id=".$rows['subject_id'];
    $topic_query=mysqli_query($con,$topic_select);
    $t=explode(',', $rows['topic']);
	$str="";
	foreach($t as $key=>$val){$str.="'".$val."',";}
	$frest= rtrim($str,',');
    $subtopic_select="select * from tbl_syllabus where topic IN (".$frest.")";
    $subtopic_query=mysqli_query($con,$subtopic_select);

 }
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Exam</title>
<link rel="stylesheet" type="text/css" href="css/style.css">
</head>

<body>
	<table  class="wraper" border="0">
		<?php include('menu2.php'); ?>
		<tr>
			<?php include('left_sidebar_managesubject.php'); ?>
			<td  height="505"  valign="top" > 
				<h1>Edit Question</h1>
				<form action="" method="post">
				<table class="table_login" width="70%" border="0"  >
				<tr>
						<td text-align="right">Subject</td>
						<td><select name="subject_id" id="subject_id" onchange="get_topic(this.value);" >
								<?php while($row=mysqli_fetch_assoc($query)) { ?>
								<option <?php  if($rows['subject_id']==$row['id']){ echo "selected"; }?> value="<?php echo $row['id']; ?>" ><?php echo $row['subject_name']; ?> 
								</option>
							<?php } ?>
							
							</select>	
						</td>
					</tr>
					<tr>
						<td text-align="right">Topic</td>
						<td><select style="height: 100px" multiple="" name="topic[]" id="topic" onchange="get_subtopic(this);"  >
								<?php while($topic=mysqli_fetch_assoc($topic_query)){ 
									$topic_array=explode(',',$rows['topic']);
									?>
									<option  <?php if(in_array($topic['topic'],$topic_array)){ echo "selected"; }  ?> value="<?php echo $topic['topic']; ?>" ><?php echo $topic['topic']; ?></option>
								<?php } ?>
							</select>	
						</td>
					</tr>
					<tr>
						<td text-align="right">Sub Topic</td>
						<td><select style="height: 100px" multiple="" name="sub_topic[]" id="sub_topic"  >
								<?php while($stopic=mysqli_fetch_assoc($subtopic_query)){ 
									$topic_array=explode(',',$rows['sub_topic']);
									?>
									<option  <?php if(in_array($topic['sub_topic'],$topic_array)){ echo "selected"; }  ?>value="<?php echo $topic['sub_topic']; ?>" ><?php echo $topic['topic']; ?></option>
								<?php } ?>
							</select>	
						</td>
					</tr>
					<tr>
						<td text-align="right">Question Type</td>
						<td><select name="question_type" id="question_type" >
								<option>--Select Type--</option>
								<option <?php if($rows['question_type']=="Descriptive"){ echo "selected"; }?>>Descriptive</option>
								<option <?php if($rows['question_type']=="Numeric"){ echo "selected"; }?> >Numeric</option>
								<option <?php if($rows['question_type']=="Multiple Choice"){ echo "selected"; }?> >Multiple Choice</option>
								 
							</select>	
						</td>
					</tr>
					<tr>
						<td text-align="right">Dificulty Level</td>
						<td><select name="difficulty_level" id="difficulty_level" >
								<option>--Select Level--</option>
								<option <?php if($rows['difficulty_level']=="Easy"){ echo "selected"; }?> >Easy</option>
								<option <?php if($rows['difficulty_level']=="Medium"){ echo "selected"; }?>>Medium</option>
								<option <?php if($rows['difficulty_level']=="Hard"){ echo "selected"; }?>>Hard</option>
							</select>	
						</td>
					</tr>
  					<tr>
						<td text-align="right">Maximum Possible Marks</td>
						<td><input type="text" required="required" size="30" value="<?php echo $rows['max_possible_marks']; ?>" name="max_possible_marks" id="max_possible_marks"></td>
					</tr>
					<tr>
						<td text-align="left">Question</td>
						<td><textarea type="text" required="required" rows="4" cols="30" name="question" id="question"><?php echo $rows['question']; ?></textarea> </td>
					</tr>
					<tr>
						<td text-align="left">Previous Years</td>
						<td>
					    <input type="text"  size="10" value="<?php echo $rows['pervious_year1']; ?>" name="pervious_year1" id="pervious_year1" />
					    <input type="text" size="10" value="<?php echo $rows['pervious_year2']; ?>" name="pervious_year2" id="pervious_year2" />
					    <input type="text"  size="10" value="<?php echo $rows['pervious_year3']; ?>" name="pervious_year3" id="pervious_year3" />
					    <input type="text"  size="10" value="<?php echo $rows['pervious_year4']; ?>" name="pervious_year4" id="pervious_year4" />
 						<input type="text"  size="10" value="<?php echo $rows['pervious_year5']; ?>" name="pervious_year5" id="pervious_year5" />
					  </td>
					</tr>
					<tr>
						<td colspan="2" align="center"><button class="btn_mouse" name="btn_submit" onclick="window.location='manage_questions.php'" type="button" style="width: 25%;height: 31px;">Cancel</button> &nbsp;<button name="btn_submit" type="submit" style="width: 25%;height: 31px;">Submit</button> </td>
					</tr>
				</table>
				</form>
			</td>
		</tr>
		<tr class="tr_row">
			<td height="20" colspan="2" bgcolor="#9F6479" align="center"><span class="style11">Copyright &copy; 2019 College of Engineering, Pune</span></td>
		</tr>
	</table>
</body>
<script type="text/javascript">
function get_topic(val) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("topic").innerHTML = this.responseText;
    }
  };
  xhttp.open("GET", "ajax.php?id="+val, true);
  xhttp.send();
}

function get_subtopic(sel) {
  var opts = [],
    opt;
  var len = sel.options.length;
  for (var i = 0; i < len; i++) {
    opt = sel.options[i];
    if (opt.selected) {
      opts.push(opt.value);
    }
  }
  if(opts){
	var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("sub_topic").innerHTML = this.responseText;
    }
   };
  xhttp.open("POST", "ajax.php", true);
  xhttp.send(opts);
}
}

</script>
</html>
