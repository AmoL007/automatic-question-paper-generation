<?php
include('session.php'); 
include('db.php');
global $con;
if(!isset($_SESSION['user_id']))
{
  header('Location:index.php');
}
if(isset($_POST['btn_submit']))
{
  print_r($_POST);
  exit;
}
 $select="select * from tbl_subject";
 $query=mysqli_query($con,$select);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Exam</title>
<link rel="stylesheet" type="text/css" href="css/style.css">
</head>

<body>
	<table  class="wraper" border="0">
		<?php include('menu2.php'); ?>
		<tr>

			<td colspan="2" align="" height="505" valign="top" style="
    padding-left: 187px;
    padding-right: 50px;
">  
				<h1>Create New Question Paper</h1>
				 
				
				<form action="get_question.php" method="post">
				 <div align="Left">
				<table class="table_login" width="960" height="133" border="0">

                   
                  <tr>
                    <td >Subject</td>
                    <td colspan="13" ><select name="subject" required="required" onchange="get_topic(this.value),get_subtopic(this.value);" >
                      <option value="">--select--</option>

                    <?php while($row=mysqli_fetch_assoc($query)) { ?>
                     <option value="<?php echo $row['id']; ?>"><?php echo $row['subject_name']; ?>                     </option>
                   <?php } ?>
                         
                    </select></td>
                  </tr>
                  <tr>
                    <td >Select Year</td>
                    <td colspan="13" ><select name="year" required="required" >
                      <option value="">--select--</option>

                        <?php  
            							$date=date('Y');
            							for($i=2018;$i<=2022;$i++){?>
                        <option value="<?php echo $i; ?>" <?php if($i==$date){ echo "selected"; } ?> ><?php echo $i; ?></option>
                        <?php } ?>
                    </select></td>
                  </tr>
                  <tr>
                    <td >Exam</td>
                    <td colspan="13" ><select name="exam_type" required="required" >
                      <option value="">--select--</option>
                        <option>Test-I</option>
                        <option>Test-II</option>
						 <option>End Sem Exam</option>
                    </select></td>
                  </tr>
                  <tr>
                    <td >Max Marks</td>
                    <td colspan="13" ><input type="text" required="required" size="30"  name="number_of_marks" id="number_of_marks" value ="" /></td>
                  </tr>
                  <tr>
                    <td colspan="14"><table class="table_login" width="100%" border="0"  >
                        <tr>
                          <td colspan="3" text-align="right"><strong>Syllabus Section</strong></td>
                        </tr>
                        <tr>
                          <td text-align="right">Topic</td>
                        </tr>
                        <tr>
                          <td  text-align="right" id="topics"></td>
                        </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td colspan="13"><span style="text-align: center;">How Many Questions? </span></td>
                  </tr>
                  <tr>
                    <td>Select Questions</td>
                    <td colspan="3"><div align="center">Easy</div></td>
                    <td colspan="2"><div align="center">Medium</div></td>
                    <td colspan="4"><div align="center">Hard</div></td>
                    <td> Total </td>
                  </tr>
                  <tr>
                    <td>Question Type</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td width="250">Marks</td>
                    <td width="60">1 Marks </td>
                    <td width="60">2 Marks</td>
                    <td width="60">3 Marks</td>
                    <td width="60">3 Marks</td>
                    <td width="60">4 Marks</td>
                    <td width="60">3 Marks</td>
                    <td width="60">4 Marks</td>
                    <td width="60">5 Marks</td>
                    <td width="60">6 Marks</td>
                    <td>&nbsp;</td>
                  </tr>
                  
                  <tr>
                    <td>Descriptive<input type="hidden" id="total_sub" value="0" name=""></td>

                    <td><input type=""  size="2" value="" id="descriptive_easy_marks_1" onblur="new_get()" name="descriptive_easy_marks_1" /></td>
                    <td><input type=""  size="2" value="" id="descriptive_easy_marks_2"  onblur="new_get()" name="descriptive_easy_marks_2" /></td>
                    <td><input type=""  size="2" value="" id="descriptive_easy_marks_3"  onblur="new_get()" name="descriptive_easy_marks_3" /></td>
                    <td><input type=""  size="2" value="" id="descriptive_medium_marks_3"  onblur="new_get()" name="descriptive_medium_marks_3" /></td>
                    <td><input type=""  size="2" value="" id="descriptive_medium_marks_4"  onblur="new_get()" name="descriptive_medium_marks_4" /></td>
                    <td  ><input type=""  size="2" value="" id="descriptive_hard_marks_3" onblur="new_get()" name="descriptive_hard_marks_3" /></td>
                    <td><input type=""  size="2" value="" id="descriptive_hard_marks_4"  onblur="new_get()" name="descriptive_hard_marks_4" /></td>
                    <td><input type=""  size="2" value="" id="descriptive_hard_marks_5" onblur="new_get()" name="descriptive_hard_marks_5" /></td>
                    <td><input type=""  size="2" value="" id="descriptive_hard_marks_6" onblur="new_get()" name="descriptive_hard_marks_6" /></td>
                    <td id="descriptive_t" >0</td>
                  </tr>
                  <tr>
                    <td>Multiple Choice</td>
                    <td><input type=""  size="2" value="" id="multiple_easy_marks_1"  onblur="new_get()"  name="multiple_easy_marks_1" /></td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td id="multipl_t">0</td>
                  </tr>
                  <tr>
                    <td>Numeric</td>
                   <td>&nbsp;</td>
                    <td><input type=""  size="2" value="" onblur="new_get()" name="numeric_easy_marks_2"  id="numeric_easy_marks_2" /></td>
                    <td>&nbsp;</td>
                    <td><input type=""  size="2" value="" onblur="new_get()" name="numeric_medium_marks_3"  id="numeric_medium_marks_3" /></td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    
                    <td><input type=""  size="2" value="" onblur="new_get()" name="numeric_hard_marks_4"  id="numeric_hard_marks_4" /></td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td id="numeric_t">0</td>
                  </tr>
                  <tr>
                    <td>Total</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
         
                    <td id="final_total">0</td>
                  </tr>
                  <tr>
                    <td colspan="3"><strong align="center">Difficulty Level</strong></td>
                    <td colspan="4"><strong align="center" >Total</strong></td>
                  </tr>
                  <tr>
                    <td>Easy</td>
                    <td colspan="3"><input type="" value="" readonly="readonly" name="easy_level" id="easy_level" />
                      %</td>
                       <td colspan="3"><input type="" value="" readonly="readonly" name="easy_total" id="easy_total" />
                      </td>
                  </tr>
                  <tr>
                    <td>Medium</td>
                    <td colspan="3"><input type="" value="" readonly="readonly" name="medium_level"  id="medium_level" />
                      %</td>
                      <td colspan="3"><input type="" value="" readonly="readonly" id="medium_total" name="medium_total" />
                      </td>
                  </tr>
                  <tr>
                    <td>Hard </td>
                    <td colspan="3"><input type="" value="" readonly="readonly" name="hard_level" id="hard_level"/>
                      %</td>
                      <td colspan="3"><input type="" value="" readonly="readonly"  id="hard_total" name="hard_total" />
                      </td>
                  </tr>
                  <tr>
                    <td colspan="14"><div align="center">
                      <button name="btn_submit" type="submit"   style="width: 25%;height: 31px;">Get Questions</button>
                    </div></td>
                  </tr>
                </table>
				 
				</div>
        <input type="hidden" name="descriptive_ts" id="descriptive_ts" value="0">
        <input type="hidden" name="final_totals" id="final_totals" value="0">
        <input type="hidden" name="multipl_ts" id="multipl_ts" value="0">
        <input type="hidden" name="numeric_ts" id="numeric_ts" value="0">

				</form>
			</td>
		</tr>
		<tr class="tr_row">
			<td height="20" colspan="2" bgcolor="#9F6479" align="center"><span class="style11">Copyright &copy; 2019 College of Engineering, Pune</span></td>
		</tr>
	</table>
  <script type="text/javascript">
function get_topic(val) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("topics").innerHTML = this.responseText;
    }
  };
  xhttp.open("GET", "ajax_generate.php?id="+val, true);
  xhttp.send();
}
function get_subtopic(val) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("sub_topics").innerHTML = this.responseText;
    }
  };
  xhttp.open("GET", "ajax_generate.php?sid="+val, true);
  xhttp.send();
}
function new_get()
{
    var d_1=t(document.getElementById("descriptive_easy_marks_1").value) ;
    var d_2=t(document.getElementById("descriptive_easy_marks_2").value)*2;
    var d_3=t(document.getElementById("descriptive_easy_marks_3").value)*3;
    var d_m_3=t(document.getElementById("descriptive_medium_marks_3").value)*3;
    var d_m_4=t(document.getElementById("descriptive_medium_marks_4").value)*4;
    var d_h_3=t(document.getElementById("descriptive_hard_marks_3").value)*3;
    var d_h_4=t(document.getElementById("descriptive_hard_marks_4").value)*4;
    var d_h_5=t(document.getElementById("descriptive_hard_marks_5").value)*5;
    var d_h_6=t(document.getElementById("descriptive_hard_marks_6").value)*6;
    var m_e_1=t(document.getElementById("multiple_easy_marks_1").value);
    var n_e_2=t(document.getElementById("numeric_easy_marks_2").value)*2;
    var n_m_3=t(document.getElementById("numeric_medium_marks_3").value)*3;
    var n_h_4=t(document.getElementById("numeric_hard_marks_4").value)*4;
    var total_d=0;
    var total_n=0;
    total_d=d_1+d_2+d_3+d_m_3+d_m_4+d_h_3+d_h_4+d_h_5+d_h_6;
    total_n=n_e_2+n_m_3+n_h_4;
    //console.log(total_d);
    document.getElementById('descriptive_t').innerHTML=total_d;
    document.getElementById('numeric_t').innerHTML=total_n;
    document.getElementById('multipl_t').innerHTML=m_e_1;
    document.getElementById('descriptive_ts').value=total_d;
    document.getElementById('numeric_ts').value=total_n;
    document.getElementById('multipl_ts').value=m_e_1;
    var easy_t=d_1+d_2+d_3+m_e_1+n_e_2;
    var medium_t=d_m_3+d_m_4+n_m_3;
    var hard_t=d_h_3+d_h_4+d_h_5+d_h_6+n_h_4;
    document.getElementById('easy_total').value=easy_t;
    document.getElementById('medium_total').value=medium_t;
    document.getElementById('hard_total').value=hard_t;
    var final_total=easy_t+medium_t+hard_t;
    console.log(final_total);
    document.getElementById('final_totals').value=final_total;
    document.getElementById('final_total').innerHTML=final_total;
    document.getElementById('easy_level').value =((100/final_total)*easy_t).toFixed(2);
    document.getElementById('medium_level').value =((100/final_total)*medium_t).toFixed(2);
    document.getElementById('hard_level').value =((100/final_total)*hard_t).toFixed(2);
}
function t(a)
{
   if(a!="")
   {
    return parseInt(a);
   }else{
    return 0;
   }
}
</script>
</body>
</html>
