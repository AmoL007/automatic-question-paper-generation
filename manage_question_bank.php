<?php
include('session.php'); 
include('db.php');
if(!isset($_SESSION['user_id']))
{
	header('Location:index.php');
}
 $select="select * from tbl_subject";
 $query=mysqli_query($con,$select);

 $select1="select * from  tbl_question_bank group by unit";
 $query1=mysqli_query($con,$select1);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Exam</title>
<link rel="stylesheet" type="text/css" href="css/style.css">
</head>

<body>
	<table  class="wraper" border="0">
		<?php include('menu2.php'); ?>
		<tr>
			<?php include('left_sidebar_managesubject.php'); ?>
			<td  height="505"  valign="top" > 
				<h1>Search</h1>
				<form action="manage_questions.php" method="post">
				<table class="table_login" width="70%" border="0"  >
				<tr>
						<td text-align="right">Subject</td>
						<td><select name="subject_id" id="subject_id" required="required" onchange="get_topic(this.value);" >
							   <option value="">--select--</option>
								<?php while($row=mysqli_fetch_assoc($query)) { ?>
								<option value="<?php echo $row['id']; ?>" ><?php echo $row['subject_name']; ?> 
								</option>
							<?php } ?>
							</select>	
						</td>
					</tr>
						<tr>
						<td text-align="right">Unit</td>
						<td><select name="unit" >
								<?php while($row1=mysqli_fetch_assoc($query1)) { ?>
								<option value="<?php echo $row1['unit']; ?>" ><?php echo $row1['unit']; ?> 
								</option>
							<?php } ?>
							</select>	
						</td>
					</tr>
					<tr>
						<td text-align="right">Topic</td>
						<td><select style="height: 100px" multiple="" name="topic[]" id="topic"   >
								<option>--select--</option>
							</select>		
						</td>
					</tr>
				 
					<tr>
						<td colspan="2" align="center"><button type="button" class="btn_mouse" name="btn_cancel" onclick="window.location='manage_questions.php'" type="button" style="width: 25%;height: 31px;">Cancel</button> &nbsp;<button class="btn_mouse" name="btn_submit" type="submit" style="width: 25%;height: 31px;">Search</button> </td>
					</tr>

				</table>
			

				</form>
			</td>
		</tr>
		<tr class="tr_row">
			<td height="20" colspan="2" bgcolor="#9F6479" align="center"><span class="style11">Copyright &copy; 2019 College of Engineering, Pune</span></td>
		</tr>
	</table>
	<script type="text/javascript">
function get_topic(val) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("topic").innerHTML = this.responseText;
    }
  };
  xhttp.open("GET", "ajax.php?id="+val, true);
  xhttp.send();
}



</script>
</body>
</html>
