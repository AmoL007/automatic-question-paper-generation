<?php
include('session.php'); 
include('db.php');
if(!isset($_SESSION['user_id']))
{
	header('Location:index.php');
}
$limit = 20;  
if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };  
$start_from = ($page-1) * $limit;
 
 if(isset($_POST['btn_submit']))
 {
   $subject_where="";
   if(@$_POST['subject_id']!="")
   {
   	$subject=$_POST['subject_id'];
   	$subject_where="subject_id='".$subject."'";
   }else{
   $subject_where="";
	}
   $str="";
   if(count(@$_POST['topics'])>0){
   foreach($_POST['topics'] as $key=>$val){$str.="'".$val."',";}
   $topic= rtrim($str,',');
    $where_in_topic=" AND topic IN (".$topic.")";
   }else{
   	$where_in_topic="";
   }

   $subtr="";
   if(count(@$_POST['sub_topic'])>0){

   foreach($_POST['sub_topic'] as $key=>$val){$str.="'".$val."',";}
   $stopic= rtrim($subtr,',');
   $where_in_subtopic=" AND sub_topic IN (".$stopic.")";

    }else{
    $where_in_subtopic="";
    }


   if($_POST['unit']!="")
   {
   	$unit=$_POST['unit'];
   	$unit_where="AND unit='".$unit."'";
   }else{
   	$unit_where="";
   }
   $select="select * from tbl_question_bank where $subject_where  $where_in_topic $where_in_subtopic $unit_where  LIMIT $start_from, $limit";
 }else{
   $select="select * from tbl_question_bank LIMIT $start_from, $limit";
}
   if(isset($_GET['id']) && $_GET['action']=="delete")
	 {
	 	$select="DELETE FROM  tbl_question_bank WHERE question_id=".base64_decode($_GET['id']);
	 	$query=mysqli_query($con,$select) ;
	 	if($query){
	 	   header('Location:manage_questions.php?msg=success&action=delete');
	 	}else{
	 		header('Location:manage_questions.php?msg=error&action=delete');
	 	}
	 }
 $query=mysqli_query($con,$select);
 $num_rows=mysqli_num_rows($query);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Exam</title>
<link rel="stylesheet" type="text/css" href="css/style.css">
</head>

<body>
	<table  class="wraper" border="0">
		<?php include('menu2.php'); ?>
		<tr width="100%">
			<?php include('left_sidebar_managesubject.php'); ?>
			<td  width="80%" height="505"    valign="top" class="td_m" > 
				<h3>Manage Questions</h3>

				<button  onclick="window.location='add_question.php'" style="float: right;margin-bottom: 17px;" >Add New Question</button>
								<?php if(isset($_GET['msg'])){ 
						$msg="";
								if($_GET['msg']=="success"){
									$action=$_GET['action'];
									if($action=="update"){
										$msg="Question updated successfully";
									}else if($action=="add"){
										$msg="Question added successfully";

									}else if($action=="delete"){
										$msg="Question deleted successfully";
									}else if($action=="status")
									{
										$msg="Question status change successfully";
									}
								?>
									<div class="alert success clearfix">
									  <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
									 <?php echo $msg; ?>
									</div>
						    <?php } }?>
				<form action="" method="post">
						<table class="table_manage" width="100%" style="margin-left: 2px; margin-right:2px; margin-bottom: 20px" border="0">
					<tr>
						<th>QID</th>
						<th>Question Name</th>
						<th>Question Type</th>
						<th>Difficulty</th>
						<th>Max Possible Marks</th>
						<th>Date Added</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
					<?php
					if($num_rows>0){
					$i=1;

					 while($row=mysqli_fetch_assoc($query)){ ?>
					<tr>
						<td><?php echo $row['question_id']; ?></td>
						<td><?php echo nl2br($row['question']); ?></td>
						<td><?php echo $row['question_type']; ?></td>
						<td><?php echo $row['difficulty_level']; ?></td>
						<td><?php echo $row['max_possible_marks']; ?></td>
						<td><?php echo date('d M Y',strtotime($row['created_date'])); ?></td>
						<td>Active</td>
						<td><a href="edit_question.php?id=<?php echo base64_encode($row['question_id']); ?>" style="cursor:pointer" ><img width="20" src="img/edit.png" /></a>&nbsp;&nbsp;<a href="manage_questions.php?id=<?php echo base64_encode($row['question_id']); ?>&action=delete" ><img width="20" src="img/delete.png" /></a></td>
					</tr>
				<?php $i++; } }else{ ?>
					<tr>Sorry no search result found.</tr>
				<?php } ?>
					
				</table>
				<?php  
					$sql = "SELECT COUNT(question_id) as cnt FROM tbl_question_bank";  
					$rs_result = mysqli_query($con, $sql);  
					$row = mysqli_fetch_assoc($rs_result);  
					$total_records = $row['cnt'];  
					$total_pages = ceil($total_records / $limit);  
					$pagLink = "<div class='pagination'>";  
					for ($i=1; $i<=$total_pages; $i++) { 
					             if($page==$i)
					             {
					             	$active="active";
					             }else{
					             	$active="";
					             } 
					             $pagLink .= "<a class='".$active."' href='manage_questions.php?page=".$i."'>".$i."</a>";  
					};  
					echo $pagLink . "</div>";  
					?>
	
				</form>
			</td>
		</tr>
		<tr class="tr_row">
			<td height="20" colspan="2" bgcolor="#9F6479" align="center"><span class="style11">Copyright &copy; 2019 College of Engineering, Pune</span></td>
		</tr>
	</table>
</body>
</html>
